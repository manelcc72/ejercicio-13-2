package es.manelcc.t13ej2;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private Button btnHablar, btnParar, btnLimpiar;
    private EditText texto;
    private TextToSpeech tts;
    private SeekBar picht, speech;
    private TextView txtPicht, txtSpeech;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtSpeech = (TextView) findViewById(R.id.txtSpeech);
        txtPicht = (TextView) findViewById(R.id.txtPicht);
        picht = (SeekBar) findViewById(R.id.seekBarPicht);
        speech = (SeekBar) findViewById(R.id.seekBarSpeech);
        texto = (EditText) findViewById(R.id.texto);
        btnHablar = (Button) findViewById(R.id.btnHablar);
        btnParar = (Button) findViewById(R.id.btnParar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        tts = new TextToSpeech(this, new TtsListener());

        picht.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                tts.setPitch(progress);
                txtPicht.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                txtPicht.setText("Picht");
            }
        });

        speech.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                tts.setSpeechRate(progress);
                txtSpeech.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                txtSpeech.setText("Speech");
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                texto.setText("");
            }
        });

        btnParar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tts != null) {
                    tts.stop();
                }
            }
        });

        btnHablar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tts != null && !texto.getText().equals("")) {
                    tts.speak(texto.getText().toString(),
                            TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });


    }

    private class TtsListener implements TextToSpeech.OnInitListener {
        @Override
        public void onInit(int status) {
            if (status == TextToSpeech.SUCCESS) {
                if (tts.isLanguageAvailable(Locale.getDefault()) > 0) {
                    tts.setLanguage(Locale.getDefault());
                    //valor de tono
                    tts.setPitch(0.8f);
                    //velocidad
                    tts.setSpeechRate(1.1f);
                    Toast.makeText(MainActivity.this, "TTS iniciado",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}

